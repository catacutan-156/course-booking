import { useState, useEffect } from 'react';
import AppNavBar from './components/AppNavBar';

//acquire the pages that will make up the app
import Home from './pages/Home';
import Register from './pages/Register';
import LoginPage from './pages/Login';
import Catalog from './pages/Courses';
import ErrorPage from './pages/Error';
import CourseView from './pages/CourseView';
import AddCourse  from './pages/AddCourse';
import UpdateCourse from './pages/UpdateCourse';
import Logout from './pages/Logout';

import { UserProvider } from './UserContext';

//implement page routing in our page
//acquire the utilities from react-router-dom
import { BrowserRouter as Router , Routes, Route } from 'react-router-dom';
//BrowserRouter -> this is standard library component for router in react. this enable navagition amongst views of various components. It will serve as the 'parent' component that will be used to store all the other components.
// as -> alias leyword in JS.
//Routes -> it's a new component introduced in V^ of react-router-dom whose task is allow switching between locations.

//JSX component -> self closing tag
//syntax: <element />
import './App.css';

function App() {

  // the role of the Provider was assigned to the App.js file, which means all the information that we will declare here will automatically become a global scope.
  // Initialize a state of the user to provide/identify the status of client who is using the app.
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  // Create a function that will unset/unmount the user
  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null
    })
  }

  useEffect(() => {
    let token = localStorage.getItem('accessToken');

    fetch("https://protected-beyond-20929.herokuapp.com/users/details", {
        headers: {
          Authorization: `Bearer ${token}`
        }
    }).then(res => res.json()).then(convertedData => {
      if (typeof convertedData._id !== "undefined") {
        setUser({
          id: convertedData._id,
          isAdmin: convertedData.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[user]);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
         <AppNavBar />
         <Routes>
            <Route path='/' element={<Home />} /> 
            <Route path='/register' element={<Register />}  />
            <Route path='/courses' element={<Catalog />} /> 
            <Route path='*' element={<ErrorPage/>} />
            <Route path= '/login' element={<LoginPage />} />
            <Route path='/courses/view/:id' element={<CourseView/>} />
            <Route path='/logout' element={<Logout />} />
            <Route path='/add/courses' element={<AddCourse/>} />
            <Route path='/update/courses' element={<UpdateCourse/>} />
         </Routes>
      </Router>
    </UserProvider>
  );
};

export default App;
