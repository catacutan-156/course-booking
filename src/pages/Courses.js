//Identify which components will be displayed
import { useState, useEffect } from 'react'
import Hero from './../components/Banner'; 
import CourseCard from './../components/CourseCard';
import { Container } from 'react-bootstrap';

const bannerDetails = {
   title: 'Course Catalog',
   content: 'Browse through our Catalog of Courses'
}

// Catalog all active courses
	// 1. Container for each course that we will retrieve from the database.
	// 2. Declare a state for the courses collection, by default, it is an empty array.
	// 3. Create a side effect to send a request to our backend API project.
	// 4. Pass down the retrieve resources inside the component as props.

export default function Courses() {

	// this array/storage will be used to save the course components for all active courses.
	const [coursesCollection, setCourseCollection] = useState([]);

	// create an effect using the Effect Hook, provide a list of dependencies.
	// the effect that we are going to create is fetching data from the server.
	useEffect(() => {
		// fetch() - JS method which allows you to pass/create a request to an API.
		// Syntax: fetch(<request URL>, {OPTIONS})
		// GET HTTP METHOD especially since we do not need to insert a req body or pass a token - no need to insert an options parameter

		// Remember that once you send a request to an endpoint, a promise is returned as a result.
		// Make the response usable on the frontend side.
		// Upon converting the fetched data to a json format, a new promise will be executed.
		fetch("https://protected-beyond-20929.herokuapp.com/courses/").then(res => res.json()).then(convertedData => {
			// we want the resources ot be displayed in the page.
			// since the data is stored in an array storage, we will iterate the array to explicitly get each document one by one.
			setCourseCollection(convertedData.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course}/>	
				)
			})) 
		});
	}, []);

	return(
		<>
			<Hero bannerData={bannerDetails} />
			<Container>
				{coursesCollection}
			</Container>
		</>
	);
};